%global srcname bokeh
%global sum Statistical and novel interactive HTML plots for Python

Name:           python-%{srcname}
Version:        0.12.4
Release:        1%{?dist}
Summary:        %{sum}

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/b/%{srcname}/%{srcname}-%{version}.tar.gz
Source1:        %{srcname}-README.md
Patch0001:      %{srcname}-0001-TST-Skip-tests-with-unavailable-optional-deps.patch
Patch0002:      %{srcname}-0002-TST-Don-t-assume-current-dir-is-source-directory.patch

BuildArch:      noarch
BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-pytest python3-pytest
BuildRequires:  python2-six >= 1.5.2
BuildRequires:  python3-six >= 1.5.2
BuildRequires:  python2-requests >= 1.2.3
BuildRequires:  python3-requests >= 1.2.3
BuildRequires:  PyYAML >= 3.10
BuildRequires:  python3-PyYAML >= 3.10
BuildRequires:  python2-dateutil >= 2.1
BuildRequires:  python3-dateutil >= 2.1
BuildRequires:  python2-jinja2 >= 2.7
BuildRequires:  python3-jinja2 >= 2.7
BuildRequires:  python2-numpy >= 1.7.1
BuildRequires:  python3-numpy >= 1.7.1
BuildRequires:  python2-tornado >= 4.3
BuildRequires:  python3-tornado >= 4.3
BuildRequires:  python2-futures >= 3.0.3
BuildRequires:  python2-pandas python3-pandas
BuildRequires:  python2-mock python3-mock

%description
Bokeh, a Python interactive visualization library, enables beautiful and
meaningful visual presentation of data in modern web browsers. With Bokeh, you
can quickly and easily create interactive plots, dashboards, and data
applications.

Bokeh helps provide elegant, concise construction of novel graphics in the
style of D3.js, while also delivering high-performance interactivity over very
large or streaming datasets.


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-six >= 1.5.2
Requires:       python2-requests >= 1.2.3
Requires:       PyYAML >= 3.10
Requires:       python2-dateutil >= 2.1
Requires:       python2-jinja2 >= 2.7
Requires:       python2-numpy >= 1.7.1
Requires:       python2-tornado >= 4.3
Requires:       python2-futures >= 3.0.3
Recommends:     python2-pandas

%description -n python2-%{srcname}
Bokeh, a Python interactive visualization library, enables beautiful and
meaningful visual presentation of data in modern web browsers. With Bokeh, you
can quickly and easily create interactive plots, dashboards, and data
applications.

Bokeh helps provide elegant, concise construction of novel graphics in the
style of D3.js, while also delivering high-performance interactivity over very
large or streaming datasets.


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-six >= 1.5.2
Requires:       python3-requests >= 1.2.3
Requires:       python3-PyYAML >= 3.10
Requires:       python3-dateutil >= 2.1
Requires:       python3-jinja2 >= 2.7
Requires:       python3-numpy >= 1.7.1
Requires:       python3-tornado >= 4.3
Recommends:     python3-pandas

%description -n python3-%{srcname}
Bokeh, a Python interactive visualization library, enables beautiful and
meaningful visual presentation of data in modern web browsers. With Bokeh, you
can quickly and easily create interactive plots, dashboards, and data
applications.

Bokeh helps provide elegant, concise construction of novel graphics in the
style of D3.js, while also delivering high-performance interactivity over very
large or streaming datasets.


%prep
%autosetup -n %{srcname}-%{version} -p1
cp %SOURCE1 README.md


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py3_install
mv %{buildroot}%{_bindir}/bokeh %{buildroot}%{_bindir}/bokeh-%{python3_version}
ln -s bokeh-%{python3_version} %{buildroot}%{_bindir}/bokeh-3

%py2_install
mv %{buildroot}%{_bindir}/bokeh %{buildroot}%{_bindir}/bokeh-%{python2_version}
ln -s bokeh-%{python2_version} %{buildroot}%{_bindir}/bokeh-2
ln -s bokeh-2 %{buildroot}%{_bindir}/bokeh


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-%{python2_version} -ra -m 'not (examples or integration or quality)' bokeh
LANG=C.UTF-8 PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} -ra -m 'not (examples or integration or quality)' bokeh


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.md
%{_bindir}/bokeh
%{_bindir}/bokeh-2
%{_bindir}/bokeh-%{python2_version}
%{python2_sitelib}/bokeh*


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.md
%{_bindir}/bokeh-3
%{_bindir}/bokeh-%{python3_version}
%{python3_sitelib}/bokeh*


%changelog
* Tue Feb 28 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.4-1
- Initial package release.
